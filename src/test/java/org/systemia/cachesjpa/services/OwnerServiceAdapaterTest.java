package org.systemia.cachesjpa.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.systemia.jpa_cache_dtos.domain.OwnerServiceAdapter;
import org.systemia.jpa_cache_dtos.entity.Pet;
import org.systemia.jpa_cache_dtos.infrastructure.persistence.PetRepository;

@ContextConfiguration
@ExtendWith(SpringExtension.class)
class OwnerServiceAdapaterTest {

	@Mock
	private PetRepository repository;

	@InjectMocks
	private OwnerServiceAdapter service;

	@Autowired
	private CacheManager cacheManager;

	private List<Pet> petsMock;

	@BeforeEach
	void setUp() {
		cacheManager.getCache("test-cache").clear();

		petsMock = new ArrayList<>(
				List.of(Pet.builder().id(1).name("Pet 1").build()));
	}

	@AfterEach
	void tearDown() {
		petsMock.clear();
	}

	@Test
	void isntNullableMocks() {
		assertNotNull(repository);
		assertNotNull(service);
	}

	@Test
	void givenRequestAllListPets_thenReturnListFromRepository() {
		when(repository.findAll()).thenReturn(petsMock);

		List<Pet> actual = service.findAllPets();

		assertThat(actual).isNotEmpty();
		assertEquals(1, actual.size());
		assertThat(actual).hasSize(1);
	}

	@Test
	void givenCacheableMethod_whenCalledTwice_thenResultShouldBeFromcCache() {
		when(repository.findAll()).thenReturn(petsMock);

		// Act
		var firstCall = service.findAllPetsCache();
		var secondCall = service.findAllPetsCache();

		assertThat(firstCall).isSameAs(secondCall);
		verify(repository, times(2)).findAll();
	}

	@Configuration
	@EnableCaching
	static class TestCacheConfig {
		@Bean
		CacheManager cacheManager() {
			return new ConcurrentMapCacheManager("test-cache");
		}
	}
}
