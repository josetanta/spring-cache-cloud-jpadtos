package org.systemia.jpa_cache_dtos.specification;

import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.Predicate;
import org.springframework.data.jpa.domain.Specification;
import org.systemia.jpa_cache_dtos.entity.Owner;
import org.systemia.jpa_cache_dtos.entity.Pet;
import org.systemia.jpa_cache_dtos.filters.PetRequestFilters;

import java.util.ArrayList;
import java.util.Objects;

public class PetSpecs {

	public static Specification<Pet> filtering(PetRequestFilters filters) {
		return (petEntity, query, cb) -> {
			var predicates = new ArrayList<Predicate>();

			if (!Objects.isNull(filters.name())) {
				predicates.add(cb.like(petEntity.get(Pet.C_NAME),
					"%" + filters.name() + "%"));
			}

			if (!Objects.isNull(filters.owner())) {
				Join<Owner, Pet> petOwner = petEntity.join("owner");
				predicates.add(cb.like(petOwner.get("firstname"),
					"%" + filters.owner() + "%"));
			}

			return cb.and(predicates.toArray(new Predicate[0]));
		};
	}

}
