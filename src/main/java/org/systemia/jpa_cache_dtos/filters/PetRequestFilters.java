package org.systemia.jpa_cache_dtos.filters;

public record PetRequestFilters(
	String name,
	String owner
) {

}
