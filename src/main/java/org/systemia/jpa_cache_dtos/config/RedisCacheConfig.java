package org.systemia.jpa_cache_dtos.config;

import io.netty.util.internal.shaded.org.jctools.queues.MessagePassingQueue.Supplier;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;

import java.time.Duration;

@Configuration
@EnableCaching
public class RedisCacheConfig {

	private final Supplier<RedisCacheConfiguration> cacheConfiguration = () -> RedisCacheConfiguration
		.defaultCacheConfig()
		.entryTtl(Duration.ofMinutes(1))
		.disableCachingNullValues();

	@Bean
	RedisCacheManager cacheManager(RedisConnectionFactory factory) {
		return RedisCacheManager.builder(factory)
			.cacheDefaults(cacheConfiguration.get())
			.transactionAware()
			.build();
	}
//	@Bean
//	RedisCacheManagerBuilderCustomizer redisCacheManagerBuilderCustomizer() {
//		return builder -> builder.withCacheConfiguration("caches-app",
//				RedisCacheConfiguration.defaultCacheConfig()
//						.entryTtl(Duration.ofMinutes(20)));
//	}

//	public RedisCacheManager cacheManager(
//			RedisConnectionFactory connectionFactory) {
//		ObjectMapper mapper = new ObjectMapper();
//		mapper.enable(JsonGenerator.Feature.IGNORE_UNKNOWN);
//		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
//		mapper.activateDefaultTyping(mapper.getPolymorphicTypeValidator(),
//				ObjectMapper.DefaultTyping.NON_FINAL, JsonTypeInfo.As.PROPERTY);
//
//		RedisSerializer<Object> serializer = new GenericJackson2JsonRedisSerializer(
//				mapper);
//
//		RedisCacheConfiguration config = RedisCacheConfiguration
//				.defaultCacheConfig()
//				.serializeKeysWith(RedisSerializationContext.SerializationPair
//						.fromSerializer(new StringRedisSerializer()))
//				.serializeValuesWith(RedisSerializationContext.SerializationPair
//						.fromSerializer(serializer))
//				.entryTtl(Duration.ofMinutes(1));
//		
//		return RedisCacheManager.builder(connectionFactory)
//				.cacheDefaults(config)
//				.build();
//	}
}
