package org.systemia.jpa_cache_dtos.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.systemia.jpa_cache_dtos.utils.PageSerializer;

@Configuration
public class JacksonConfig {

	@Bean
	public ObjectMapper objectMapper(Jackson2ObjectMapperBuilder builder) {
		ObjectMapper objectMapper = builder.build();
//		objectMapper.addMixIn(Page.class, PageMixin.class);
		SimpleModule module = new SimpleModule();
		module.addSerializer(Page.class, new PageSerializer());
		objectMapper.registerModule(module);
		return objectMapper;
	}
}
