package org.systemia.jpa_cache_dtos.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@EnableAspectJAutoProxy
@ComponentScan({"org.systemia.jpa_cache_dtos.*"})
public class AOPConfig {

}
