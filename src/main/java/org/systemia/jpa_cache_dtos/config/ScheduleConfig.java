package org.systemia.jpa_cache_dtos.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.List;
import java.util.Objects;

@Configuration
@EnableScheduling
@Slf4j
@RequiredArgsConstructor
public class ScheduleConfig {

	private final CacheManager cacheManager;

	@Scheduled(cron = "${cron.show_caches}")
	public void showCaches() {
		List<String> allCaches = cacheManager.getCacheNames().stream()
			.map(nameCache -> nameCache + " -> [" + Objects.requireNonNull(cacheManager.getCache(nameCache)).getName() + "]")
			.toList();

		log.info("[REDIS-CACHE] {}", allCaches);
	}
}
