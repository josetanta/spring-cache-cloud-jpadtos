package org.systemia.jpa_cache_dtos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.systemia.jpa_cache_dtos.entity.Owner;
import org.systemia.jpa_cache_dtos.entity.Pet;
import org.systemia.jpa_cache_dtos.infrastructure.persistence.OwnerRepository;
import org.systemia.jpa_cache_dtos.infrastructure.persistence.PetRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

@SpringBootApplication
@EnableFeignClients
@EnableSpringDataWebSupport
public class MainApplication {

	@Autowired
	private PetRepository petRepository;

	@Autowired
	private OwnerRepository ownerRepository;

	public static void main(String[] args) {
		SpringApplication.run(MainApplication.class, args);
	}

	@Bean
	CommandLineRunner commandLineRunner() {
		return args -> {
			List<Pet> pets = new ArrayList<>();
			ownerRepository.save(Owner.builder()
				.firstname("Owner_" + 1)
				.lastname("owner_last" + 1)
				.build());
			ownerRepository.save(Owner.builder()
				.firstname("Owner_" + 2)
				.lastname("owner_last" + 2)
				.build());

			IntStream.range(1, 100).forEach(num -> {
				pets.add(Pet.builder().id(num).name("Pet " + num)
					.owner(Owner.builder().id(ThreadLocalRandom.current().nextInt(1, 3)).build()).build());
			});
			petRepository.saveAll(pets);
		};
	}

}
