package org.systemia.jpa_cache_dtos.services.feign;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.systemia.jpa_cache_dtos.rest.dtos.ProductDTO;

import java.util.Optional;

@FeignClient(
	name = "productStoreService",
	url = "${services.store-api}",
	path = "/products"
)
public interface ProductStoreService {

	@GetMapping(path = "/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE})
	@Cacheable(cacheNames = "products-store")
	Optional<ProductDTO> findProductByID(@PathVariable("id") Integer id);
}
