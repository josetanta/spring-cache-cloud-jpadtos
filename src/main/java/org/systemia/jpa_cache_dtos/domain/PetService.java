package org.systemia.jpa_cache_dtos.domain;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.systemia.jpa_cache_dtos.entity.Pet;
import org.systemia.jpa_cache_dtos.filters.PetRequestFilters;
import org.systemia.jpa_cache_dtos.utils.PetDTO;

import java.util.List;

public interface PetService {

	List<Pet> findAllPets();

	List<Pet> findAllPetsCache();

	Page<PetDTO> findAndFiltering(Pageable pageable, PetRequestFilters filters);

	void cleanEntriesCaches();

	Page<PetDTO> findAllPetsDTO(int page, int size);

	Page<Pet> findAllPetsCatchPages(Pageable pageable);
}
