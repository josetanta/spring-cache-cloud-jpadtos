package org.systemia.jpa_cache_dtos.domain;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.systemia.jpa_cache_dtos.entity.Pet;
import org.systemia.jpa_cache_dtos.filters.PetRequestFilters;
import org.systemia.jpa_cache_dtos.infrastructure.persistence.PetRepository;
import org.systemia.jpa_cache_dtos.specification.PetSpecs;
import org.systemia.jpa_cache_dtos.utils.PetDTO;

import java.util.List;

@Service("ownerService")
@Slf4j
@AllArgsConstructor
public class OwnerServiceAdapter implements PetService {

	private final PetRepository petRepository;

	public List<Pet> findAllPets() {
		return petRepository.findAll();
	}

	@Cacheable(value = "pets", key = "'allpets'")
	public List<Pet> findAllPetsCache() {
		return petRepository.findAll();
	}

	// @Cacheable(value = "pets", key = "'allpetspages_page_' + #page + '_size_' + #size", unless = "#result == null")
	public Page<Pet> findAllPetsCatchPages(Pageable pageable) {
		return petRepository.findAll(pageable);
	}

	// @Cacheable(value = "pets-dto", key = "'allpetspagesdto_page_' + #page +
	// '_size_' + #size", unless = "#result == null")
	public Page<PetDTO> findAllPetsDTO(int page, int size) {
		Pageable pageable = PageRequest.of(page, size);
		return petRepository.findAllPetsDTO(pageable);
	}

	@Scheduled(cron = "0 */1 * * * *")
	@CacheEvict(allEntries = true, cacheNames = {"pets-dto", "pets"})
	public void cleanEntriesCaches() {
		log.info("[CACHE] clean all entries");
	}

	@Transactional(readOnly = true)
	public Page<PetDTO> findAndFiltering(Pageable pageable, PetRequestFilters filters) {
		Specification<Pet> spec = PetSpecs.filtering(filters);
		Page<Pet> all = petRepository.findAll(spec, pageable);

		List<PetDTO> dots = all.getContent().stream()
			.map(pet -> new PetDTO(pet.getName(), pet.getOwner().getFirstname()))
			.toList();
		Pageable pageRequest = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize());
		return new PageImpl<>(dots, pageRequest, all.getTotalElements());
	}
}
