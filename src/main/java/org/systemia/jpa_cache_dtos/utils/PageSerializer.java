package org.systemia.jpa_cache_dtos.utils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.springframework.data.domain.Page;

import java.io.IOException;

public class PageSerializer extends StdSerializer<Page> {

	public PageSerializer() {
		super(Page.class);
	}

	@Override
	public void serialize(Page page, JsonGenerator gen, SerializerProvider serializerProvider) throws IOException {
		gen.writeStartObject();
		gen.writeObjectField("data", page.getContent());
		gen.writeNumberField("totalElements", page.getTotalElements());
		gen.writeNumberField("totalPages", page.getTotalPages());
		gen.writeNumberField("page", page.getNumber());
		gen.writeNumberField("pageSize", page.getSize());
		gen.writeEndObject();
	}
}
