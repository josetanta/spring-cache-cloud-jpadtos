package org.systemia.jpa_cache_dtos.utils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonIgnoreProperties({ "pageable", "empty", "sort", "first", "last", "number" })
public abstract class PageMixin<T> {

	@JsonProperty("data")
	private List<T> content;

	private long totalElements;
}
