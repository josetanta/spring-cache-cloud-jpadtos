package org.systemia.jpa_cache_dtos.utils;

import java.io.Serializable;

/**
 * DTO for entity Pet
 */
public record PetDTO(
	String name,
	String owner
) implements Serializable {

}
