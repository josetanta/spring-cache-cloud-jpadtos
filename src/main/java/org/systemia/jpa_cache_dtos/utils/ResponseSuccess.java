package org.systemia.jpa_cache_dtos.utils;

/**
 * Response for status successfully
 * @param <T>
 */
public record ResponseSuccess<T>(
	String message,
	int status,
	boolean success,
	T data
) {
		
}
