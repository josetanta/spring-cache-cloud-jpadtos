package org.systemia.jpa_cache_dtos.utils;

/**
 * Response for status error
 * @param <T>
 */
public record ResponseError<T>(
	String message,
	int status,
	boolean error,
	T details
) {
	
}
