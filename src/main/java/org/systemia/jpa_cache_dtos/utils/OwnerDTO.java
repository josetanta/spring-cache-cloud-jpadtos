package org.systemia.jpa_cache_dtos.utils;

import java.io.Serializable;

/**
 * Owner DTO for entity Owner
 */
public record OwnerDTO(
	String firstname,
	String lastname
) implements Serializable {

}
