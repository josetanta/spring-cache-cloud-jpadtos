package org.systemia.jpa_cache_dtos.infrastructure.persistence;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.systemia.jpa_cache_dtos.entity.Pet;
import org.systemia.jpa_cache_dtos.utils.PetDTO;

import java.util.List;

public interface PetRepository extends JpaRepository<Pet, Integer>, JpaSpecificationExecutor<Pet> {
	
	/**
	 * Find all pets
	 * @return List of pets
	 */
	@Query(value = """
				SELECT new org.systemia.jpa_cache_dtos.utils.PetDTO(
					p.name,
					p.owner
				)
				FROM Pet p
		""")
	List<PetDTO> findAllPets();

	/**
	 * @param pageable
	 * @return
	 */
	@Query(value = """
				SELECT new org.systemia.jpa_cache_dtos.utils.PetDTO(
					p.name,
					p.owner
				)
				FROM Pet p
		""")
	Page<PetDTO> findAllPetsDTO(Pageable pageable);

	/**
	 * @param name
	 * @param pageable
	 * @return
	 */
	@Query(value = """
				SELECT new org.systemia.jpa_cache_dtos.utils.PetDTO(
					p.name,
					new org.systemia.jpa_cache_dtos.utils.OwnerDTO(
						p.owner.firstname,
						p.owner.lastname
					)
				)
				FROM Pet p
		""")
	@Cacheable(cacheNames = "filter-pets-dto")
	Page<PetDTO> findAllByNameContaining(String name, Pageable pageable);
}
