package org.systemia.jpa_cache_dtos.infrastructure.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.systemia.jpa_cache_dtos.entity.Owner;

public interface OwnerRepository extends JpaRepository<Owner, Integer> {

}
