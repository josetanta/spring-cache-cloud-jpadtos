package org.systemia.jpa_cache_dtos.infrastructure.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.systemia.jpa_cache_dtos.infrastructure.annotations.AspectBean;

@Slf4j
@Aspect
@AspectBean
public class PetAspect {

	@Before("execution(* org.systemia.jpa_cache_dtos.domain.*.*(..))")
	public void showPetBeforeCreated() {
		log.info("FROM------------------------- Aspect");
		// log.info("ARGS {}", pjp.getArgs());
	}

}
