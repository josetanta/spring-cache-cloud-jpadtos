package org.systemia.jpa_cache_dtos.entity;

import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDate;


/**
 * CommonEntity
 */
@Getter
@Setter
@MappedSuperclass
public class CommonEntity implements Serializable {

	@Serial
	private static final long serialVersionUID = 1L;

	@Column(name = "created_at")
	@CreationTimestamp
	private LocalDate createdAt;

	@Column(name = "updated_at")
	@UpdateTimestamp
	private LocalDate updatedAt;
}
