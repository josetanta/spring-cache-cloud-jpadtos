package org.systemia.jpa_cache_dtos.entity;

import jakarta.persistence.*;
import lombok.*;

import java.io.Serial;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Owner extends CommonEntity {

	public static final String C_ID = "id";
	public static final String C_FIRSTNAME = "firstname";
	public static final String C_LASTNAME = "lastname";

	@Serial
	private static final long serialVersionUID = -3510793898084725537L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String firstname;
	private String lastname;

	@OneToMany(orphanRemoval = true, mappedBy = "owner")
	@ToString.Exclude
	@EqualsAndHashCode.Exclude
	private List<Pet> pets;

	@Builder
	public Owner(Integer id, String firstname, String lastname) {
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
	}
}
