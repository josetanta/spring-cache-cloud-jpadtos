package org.systemia.jpa_cache_dtos.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serial;
import java.io.Serializable;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Pet implements Serializable {

	public static final String C_ID = "id";
	public static final String C_NAME = "name";
	public static final String C_REL_OWNER = "owner";
	@Serial
	private static final long serialVersionUID = -8751631574336307398L;
	@Id
	private Integer id;
	private String name;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "owner_id", nullable = false)
	private Owner owner;
}
