package org.systemia.jpa_cache_dtos.rest.dtos;

import java.io.Serializable;

/**
 * DTO for model-domain Product
 *
 * @param id
 * @param title
 * @param price
 * @param category
 * @param description
 * @param image
 */
public record ProductDTO(
	Integer id,
	String title,
	String price,
	String category,
	String description,
	String image
) implements Serializable {

}
