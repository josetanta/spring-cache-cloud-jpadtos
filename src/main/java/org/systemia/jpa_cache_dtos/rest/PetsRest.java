package org.systemia.jpa_cache_dtos.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.systemia.jpa_cache_dtos.domain.PetService;
import org.systemia.jpa_cache_dtos.filters.PetRequestFilters;
import org.systemia.jpa_cache_dtos.utils.PetDTO;

@Slf4j
@RestController
@RequiredArgsConstructor
public class PetsRest {

	@Qualifier("petService")
	private final PetService petService;

	@GetMapping("/without-cache")
	public ResponseEntity<?> getPetsRandomWithoutCache() {
		return ResponseEntity.ok(petService.findAllPets());
	}

	@GetMapping("/with-cache")
	public ResponseEntity<?> getPetsRandomWithCache() {
		return ResponseEntity.ok(petService.findAllPetsCache());
	}

	@GetMapping("/with-pages")
	public ResponseEntity<?> getPetsRandomWithCachePagination(
			Pageable pageable) {
		return ResponseEntity.ok(petService.findAllPetsCatchPages(pageable));
	}

	@GetMapping("/dto-with-pages")
	public ResponseEntity<?> getPetsDTORandomWithCachePagination(
			@RequestParam(defaultValue = "0", name = "page") int page,
			@RequestParam(defaultValue = "10", name = "size") int size) {
		return ResponseEntity.ok(petService.findAllPetsDTO(page, size));
	}

	@PostMapping("clean-caches")
	public ResponseEntity<Void> cleanCaches() {
		petService.cleanEntriesCaches();
		return ResponseEntity.noContent().build();
	}

	@GetMapping("/page-filter")
	public ResponseEntity<?> paginationAndFiltering(
		Pageable pageable,
		PetRequestFilters filter
	) {
		log.info("Looking for all pets by {}", filter);
		Page<PetDTO> andFiltering = petService.findAndFiltering(pageable, filter);
		return ResponseEntity.ok(andFiltering);
	}
}
