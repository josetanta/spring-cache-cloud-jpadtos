package org.systemia.jpa_cache_dtos.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.systemia.jpa_cache_dtos.rest.dtos.ProductDTO;
import org.systemia.jpa_cache_dtos.services.feign.ProductStoreService;

@RestController
@RequestMapping("api/v1/products")
@RequiredArgsConstructor
public class ProductRest {

	private final ProductStoreService productStoreService;

	@GetMapping(params = {"id"})
	public ResponseEntity<ProductDTO> getProductByID(@RequestParam("id") Integer id) {
		var product = productStoreService.findProductByID(id)
			.orElseThrow(() -> new IllegalStateException("Product doesnt exist."));
		return ResponseEntity.ok(product);
	}
}
